﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class TakingDamage : MonoBehaviourPunCallbacks
{
    private float startHealth = 100;
    public float health;
    [SerializeField]
    Image HealthBar;

    void Start()
    {
        health = startHealth;
        HealthBar.fillAmount = health / startHealth;
    }

    // Update is called once per frame

    [PunRPC]
    public void TakeDamage(int Damage)
    {
        health -= Damage;
        Debug.Log(health);
        HealthBar.fillAmount = health / startHealth;
        if (health<0)
        {
            Die();
        }
    }
    private void Die()
    {
        if(photonView.IsMine)
        {
            GameManager.instance.LeaveRoom();   
        }
        
    }
}
