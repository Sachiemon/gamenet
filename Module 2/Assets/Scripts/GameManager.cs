﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviourPunCallbacks
{
    public GameObject playerPrefab;
    private GameObject[] respawnPoints;
    public static GameManager instance;
    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this.gameObject);

        }
        else
        {
            instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        respawnPoints = GameObject.FindGameObjectsWithTag("Respawn");
        int randomPoint = Random.Range(0, respawnPoints.Length);
        if(PhotonNetwork.IsConnectedAndReady)
        {
            
            PhotonNetwork.Instantiate(playerPrefab.name, respawnPoints[randomPoint].transform.position,Quaternion.identity);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }
    public override void OnLeftRoom()
    {
        SceneManager.LoadScene("LobbyScene");
    }

}
