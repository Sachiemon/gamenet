﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using UnityEngine.SceneManagement;
public class ShootingScript : MonoBehaviourPunCallbacks
{
    public Camera camera;
    public GameObject HiteffectPrefab;
    public int Score;
    [Header("Hp Stuff")]
    public float startHealth=100;
    public float currentHealth;
    public Image healthBar;
    private Animator animator;
    private GameObject[] respawnPoints;
    public Text KillText;
    
    // Start is called before the first frame update
    void Start()
    {
        animator = this.GetComponent<Animator>();
        currentHealth = startHealth;
        Score = 0;
        healthBar.fillAmount = currentHealth / startHealth;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Fire()
    {
        RaycastHit hit;
        Ray ray = camera.ViewportPointToRay(new Vector3(0.5f,0.5f));
        if(Physics.Raycast(ray,out hit ,200))
        {
           
            Debug.Log(hit.collider.gameObject.name);
            photonView.RPC("CreateHitEffects", RpcTarget.All, hit.point);
            if(hit.collider.CompareTag("Player")&& !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
            {
                if(hit.collider.gameObject.GetComponent<ShootingScript>().currentHealth-25==0)
                {
                    photonView.RPC("addScore", RpcTarget.AllBuffered,1);
                    Debug.Log(Score);
                }
                hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered,25);
            }
        }
    }
    [PunRPC]
    public void TakeDamage(int Damage,PhotonMessageInfo info)
    {
        this.currentHealth -= Damage;
        this.healthBar.fillAmount = currentHealth / startHealth;
        if(currentHealth<=0)
        {
            photonView.RPC("die", RpcTarget.AllBuffered);
            
            KillText = GameObject.Find("KillText").GetComponent<Text>();
            KillText.GetComponent<Text>().text = info.Sender.NickName + "Killed " + info.photonView.Owner.NickName;
            StartCoroutine(Delaycountdown());
           
        }
    }
    [PunRPC]
    public void CreateHitEffects(Vector3 Position)
    {
        GameObject hitEffectGameobject = Instantiate(HiteffectPrefab,Position, Quaternion.identity);
        Destroy(hitEffectGameobject, 0.2f); 
    }
    [PunRPC]
    public void die()
    {
        if(photonView.IsMine)
        {
            animator.SetBool("isDead", true);
            StartCoroutine(respawnCountdown());
        }
    }
    IEnumerator respawnCountdown()
    {
        GameObject respawnText= GameObject.Find("RespawnText");
        float respawnTime = 5.0f;
        while(respawnTime>=0.0f)
        {
            yield return new WaitForSeconds(1.0f);
            respawnTime--;

            transform.GetComponent<PlayerMovementController>().enabled = false;
            respawnText.GetComponent<Text>().text = "You are killed respawning in " + respawnTime.ToString(".00");
            

        }
        animator.SetBool("isDead", false);
        respawnText.GetComponent<Text>().text = "";
        photonView.RPC("regain", RpcTarget.AllBuffered);

    }

    IEnumerator Delaycountdown()
    {
        float time = 5.0f;
        while (time >= 0.0f)
        {
            yield return new WaitForSeconds(1.0f);
            time--;
        }
        KillText.GetComponent<Text>().text = "";
    }
    [PunRPC]
    public void regain()
    {
        respawnPoints = GameObject.FindGameObjectsWithTag("Respawn");
        int randomPoint = Random.Range(0, respawnPoints.Length);
        this.transform.position = respawnPoints[randomPoint].transform.position;
        transform.GetComponent<PlayerMovementController>().enabled = true;
        currentHealth = startHealth;
        healthBar.fillAmount = currentHealth / startHealth;
    }

    [PunRPC]
    public void addScore(int scoreAdd)
    {
        Score += scoreAdd;
        if(Score==10)
        {
            name = photonView.Owner.NickName;
            photonView.RPC("winnerView", RpcTarget.All,name);
        }
    }
    [PunRPC]
    public void winnerView(string name)
    {

        GameObject winnerText = GameObject.Find("winnerText");
        winnerText.GetComponent<Text>().text = name + "Has won the match";
        
        StartCoroutine(Delaycountdown());
        GameManager.instance.LeaveRoom();
    }
}
