﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;
using TMPro;
public class PlayerSetup : MonoBehaviourPunCallbacks
{
    public GameObject fpsModel;
    public ShootingScript shooting;
    public GameObject nonFpsModel;
    public GameObject playerUIPrefab;
    public PlayerMovementController playerMovementController;
    public Camera FpsCamera;
    private Animator animator;
    public Avatar FPSAvatar, NonFpsAvatar;
    
    [SerializeField]
    TextMeshProUGUI playerNameText;
    // Start is called before the first frame update
    void Start()
    {
        animator = this.GetComponent<Animator>();
        playerMovementController = this.GetComponent<PlayerMovementController>();
        fpsModel.SetActive(photonView.IsMine);
        nonFpsModel.SetActive(!photonView.IsMine);
        animator.SetBool("isLocalPlayer", photonView.IsMine);
        shooting = this.GetComponent<ShootingScript>(); 
 
        if(photonView.IsMine)
        {
            this.animator.avatar = FPSAvatar;
            GameObject playerControllerUI = Instantiate(playerUIPrefab);
            playerMovementController.fixedTouchField = playerControllerUI.transform.Find("RotationPanel").GetComponent<FixedTouchField>();
            playerMovementController.joystick = playerControllerUI.transform.Find("Fixed Joystick").GetComponent<Joystick>();
            playerMovementController.shootButton = playerControllerUI.transform.Find("fireButton").GetComponent<Button>();
            playerMovementController.shootButton.onClick.AddListener(shooting.Fire);
           
            FpsCamera.enabled = true;
        }
        else
        {
            playerMovementController.enabled = false;
            this.animator.avatar = NonFpsAvatar;
            GetComponent<RigidbodyFirstPersonController>().enabled = false;
            FpsCamera.enabled = false;

        }
        playerNameText.text = photonView.Owner.NickName;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
