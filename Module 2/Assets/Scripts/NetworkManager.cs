﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;

public class NetworkManager : MonoBehaviourPunCallbacks
{
    [Header("Connection Status Panel")]
    public Text connectionStatusText;
    [Header("Login UI Panel")]
    public InputField playerNameInput;
    public GameObject loginUiPanel;
    [Header("GameOptionsPanel")]
    public GameObject GameOptionsPanel;
    [Header("CreateRoomPanel")]
    public GameObject CreateRoomPanel;
    public InputField roomNameInputField;
    public InputField playerCountInputField;
    
    [Header("JoinRandomRoomPanel")]
    public GameObject JoinRandomRoomPanel;
    [Header("RoomListPanel")]
    public GameObject RoomListPanel;
    public GameObject roomItemPrefab;
    public GameObject roomListParent;

    [Header("InsideRoomPanel")]
    public GameObject InsideRoomPanel;
    public Text roomInfoText;
    public GameObject playerListPrefab;
    public GameObject playerListViewParent;
    public GameObject startGameButton;


    private Dictionary<string, RoomInfo> cachedRoomList;
    private Dictionary<string, GameObject> roomListGameObjects;
    private Dictionary<int, GameObject> playerListGameObjects;


    #region UnityFunctions
    // Start is called before the first frame update
    void Start()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
        ActivatePanel(loginUiPanel);
        playerListGameObjects = new Dictionary<int, GameObject>();
        roomListGameObjects = new Dictionary<string, GameObject>();
        cachedRoomList = new Dictionary<string, RoomInfo>();
    }

    // Update is called once per frame
    void Update()
    {
        connectionStatusText.text = "Connection status " + PhotonNetwork.NetworkClientState;
    }
    #endregion
    #region UICallbacks
    public void OnBackButtonClicked()
    {
        if(PhotonNetwork.InLobby)
        {
            PhotonNetwork.LeaveLobby();

        }
        ActivatePanel(GameOptionsPanel);
    }
    public void OnCreateRoomButton()
    {
        string roomName = roomNameInputField.text;

        if(string.IsNullOrEmpty(roomName))
        {
            roomName = "Room: " + Random.Range(1000, 10000);

        }
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = (byte)int.Parse(playerCountInputField.text);
        PhotonNetwork.CreateRoom(roomName, roomOptions);
    }
    public void OnLoginButton()
    {
        string playerName = playerNameInput.text;
        if(string.IsNullOrEmpty(playerName))
        {
            Debug.Log("Player Name invalid");
        }
        else
        {
            PhotonNetwork.LocalPlayer.NickName = playerName;
            PhotonNetwork.ConnectUsingSettings();
        }
    }
    public void OnShowRoomListButtonClicked()
    {
        if(!PhotonNetwork.InLobby)
        {
            PhotonNetwork.JoinLobby();
        }
        ActivatePanel(RoomListPanel);
    }
    public void OnLeaveGameButton()
    {
        PhotonNetwork.LeaveRoom();
    }
    public void OnJoinRandomRoomButtonClicked()
    {
        ActivatePanel(JoinRandomRoomPanel);
        PhotonNetwork.JoinRandomRoom();
    }
    public void OnStartGameButtonClicked()
    {
        PhotonNetwork.LoadLevel("GameScene");
    }
    
    #endregion
    #region PunCallbacks
    public override void OnConnected()
    {
        //Debug.Log("Connected to the internet");
    }
    public override void OnConnectedToMaster()
    {
        Debug.Log(PhotonNetwork.LocalPlayer.NickName + " connected to the photon server");
        ActivatePanel(GameOptionsPanel);
    }
    public override void OnCreatedRoom()
    {
        Debug.Log(PhotonNetwork.CurrentRoom.Name + " Created");
        ActivatePanel(InsideRoomPanel);

    }
    public override void OnJoinedRoom()
    {
        Debug.Log(PhotonNetwork.LocalPlayer.NickName + " has joined " + PhotonNetwork.CurrentRoom.Name);
        ActivatePanel(InsideRoomPanel);
        roomInfoText.text = "Room Name: " + PhotonNetwork.CurrentRoom.Name + " Current Player Count:  " + PhotonNetwork.CurrentRoom.PlayerCount + " / " +
            PhotonNetwork.CurrentRoom.MaxPlayers;
        if(playerListGameObjects==null)
        {
            playerListGameObjects = new Dictionary<int, GameObject>();
        }
        foreach(Player player in PhotonNetwork.PlayerList)
        {
            GameObject playerItem = Instantiate(playerListPrefab);
            playerItem.transform.SetParent(playerListViewParent.transform);
            playerItem.transform.localScale = Vector3.one;
            playerItem.transform.Find("PlayerNameText").GetComponent<Text>().text = player.NickName;  
            playerItem.transform.Find("PlayerIndicator").gameObject.SetActive(player.ActorNumber == PhotonNetwork.LocalPlayer.ActorNumber);
            playerListGameObjects.Add(player.ActorNumber,playerItem);
            
        }
    }
    public   override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        startGameButton.SetActive(PhotonNetwork.LocalPlayer.IsMasterClient);
        ClearRoomListGameObjects();
        foreach(RoomInfo info in roomList)
        {
            Debug.Log(info.Name);
            if (!info.IsOpen || !info.IsVisible || info.RemovedFromList)
            {
                if (cachedRoomList.ContainsKey(info.Name))
                {
                    cachedRoomList.Remove(info.Name);
                }
            }
            else
            {
                if (cachedRoomList.ContainsKey(info.Name))
                {
                    cachedRoomList[info.Name] = info;

                }
                else
                {
                    cachedRoomList.Add(info.Name, info);
                }
               

            }
          
        }
        foreach(RoomInfo info in cachedRoomList.Values)
        {
            GameObject listItem = Instantiate(roomItemPrefab);
            listItem.transform.SetParent(roomListParent.transform);
            listItem.transform.localScale = Vector3.one;

            listItem.transform.Find("RoomNameText").GetComponent<Text>().text = info.Name;
            listItem.transform.Find("RoomPlayersText").GetComponent<Text>().text = "Player Count : " + info.PlayerCount + "/ " + info.MaxPlayers;
            listItem.transform.Find("JoinRoomButton").GetComponent<Button>().onClick.AddListener(() => OnJoinRoomClicked(info.Name));
            roomListGameObjects.Add(info.Name, listItem);
        }
    }
    public override void OnLeftLobby()
    {
        ClearRoomListGameObjects();
        cachedRoomList.Clear();
    }
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        roomInfoText.text = "Room Name: " + PhotonNetwork.CurrentRoom.Name + " Current Player Count:  " + PhotonNetwork.CurrentRoom.PlayerCount + " / " +
            PhotonNetwork.CurrentRoom.MaxPlayers;
        GameObject playerItem = Instantiate(playerListPrefab);
        playerItem.transform.SetParent(playerListViewParent.transform);
        playerItem.transform.localScale = Vector3.one;
        playerItem.transform.Find("PlayerNameText").GetComponent<Text>().text = newPlayer.NickName;
        playerItem.transform.Find("PlayerIndicator").gameObject.SetActive(newPlayer.ActorNumber == PhotonNetwork.LocalPlayer.ActorNumber);
        playerListGameObjects.Add(newPlayer.ActorNumber, playerItem);
    }
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        startGameButton.SetActive(PhotonNetwork.LocalPlayer.IsMasterClient);
        roomInfoText.text = "Room Name: " + PhotonNetwork.CurrentRoom.Name + " Current Player Count:  " + PhotonNetwork.CurrentRoom.PlayerCount + " / " +
            PhotonNetwork.CurrentRoom.MaxPlayers;

        Destroy(playerListGameObjects[otherPlayer.ActorNumber]);
        playerListGameObjects.Remove(otherPlayer.ActorNumber); 
    }
    public override void OnLeftRoom()
    {

        foreach (var gameObject in playerListGameObjects.Values)
        {
            Destroy(gameObject);
        }
        playerListGameObjects.Clear();
        playerListGameObjects = null;
        ActivatePanel(GameOptionsPanel);
    }
    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.LogWarning(message);
        string roomName = "Room: " + Random.Range(1000, 10000);
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = 20;
        PhotonNetwork.CreateRoom(roomName, roomOptions);
    }
    #endregion

    #region Private Methods
    private void OnJoinRoomClicked(string roomName)
    {
        PhotonNetwork.JoinRoom(roomName);
        if(PhotonNetwork.InLobby)
        {
            PhotonNetwork.LeaveLobby();
        }
    }
    private void ClearRoomListGameObjects()
    {
        foreach(var item in roomListGameObjects.Values)
        {
            Destroy(item);
        }
        roomListGameObjects.Clear();
    }
    #endregion
    #region PublicMethods
    public void ActivatePanel(GameObject panelToBeActivated)
    {
        loginUiPanel.SetActive(panelToBeActivated.Equals(loginUiPanel));
        GameOptionsPanel.SetActive(panelToBeActivated.Equals(GameOptionsPanel));
        CreateRoomPanel.SetActive(panelToBeActivated.Equals(CreateRoomPanel));
        JoinRandomRoomPanel.SetActive(panelToBeActivated.Equals(JoinRandomRoomPanel));
        RoomListPanel.SetActive(panelToBeActivated.Equals(RoomListPanel));
        InsideRoomPanel.SetActive(panelToBeActivated.Equals(InsideRoomPanel));
    }
    #endregion
}
