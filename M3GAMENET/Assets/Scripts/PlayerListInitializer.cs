﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class PlayerListInitializer : MonoBehaviour
{

    [Header("UI References")]
    public Text PlayerNameText;
    public Button playerReadyButton;
    public Image playerReadyImage;
    private bool isPlayerReady = false;

    public void initialize(int playerId,string PlayerName)
    {
        PlayerNameText.text = PlayerName;
        if (PhotonNetwork.LocalPlayer.ActorNumber != playerId)
        {
            playerReadyButton.gameObject.SetActive(false);
        }
        else
        {
            ExitGames.Client.Photon.Hashtable initializeProperties = new ExitGames.Client.Photon.Hashtable() { { Constants.Player_Ready, isPlayerReady } };
            PhotonNetwork.LocalPlayer.SetCustomProperties(initializeProperties);
            playerReadyButton.onClick.AddListener(() =>
            {
                isPlayerReady = !isPlayerReady;
                setPlayerReady(isPlayerReady);
                ExitGames.Client.Photon.Hashtable newProperties = new ExitGames.Client.Photon.Hashtable() { { Constants.Player_Ready, isPlayerReady } };
                PhotonNetwork.LocalPlayer.SetCustomProperties(newProperties);

            });
        }
    }
    public void setPlayerReady(bool playerReady)
    {
        playerReadyImage.enabled = playerReady;
        if(playerReady)
        {
            playerReadyButton.GetComponentInChildren<Text>().text = "Ready!";
            
        }
        else
        {
            playerReadyButton.GetComponentInChildren<Text>().text = "Ready?";
        }
    }
}
