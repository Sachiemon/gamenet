﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using UnityEngine.SceneManagement;
public class ShootingScript : MonoBehaviourPunCallbacks
{
    public Camera camera;
    public GameObject HiteffectPrefab;
    public int Score;
    [Header("Hp Stuff")]
    public float startHealth=100;
    public float currentHealth;
    public int PlayersAlive;
    private GameObject[] respawnPoints;
    public Text KillText;
    public enum RaiseEventCode
    {
        EliminatedCode=0,
    }

    // Start is called before the first frame update
    void Start()
    {

        currentHealth = startHealth;
        Score = 0;
        PlayersAlive = PhotonNetwork.CurrentRoom.PlayerCount;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Fire();
        }
    }
    public void Fire()
    {
        RaycastHit hit;
        Ray ray = camera.ViewportPointToRay(new Vector3(0.5f,0.5f));
        if(Physics.Raycast(ray,out hit ,200))
        {
           
            Debug.Log(hit.collider.gameObject.name);
            photonView.RPC("CreateHitEffects", RpcTarget.All, hit.point);
            if(hit.collider.CompareTag("Player")&& !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
            {
                if(hit.collider.gameObject.GetComponent<ShootingScript>().currentHealth-25==0)
                {
                    photonView.RPC("addScore", RpcTarget.AllBuffered,1);
                    Debug.Log(Score);
                }
                hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered,25);
            }
        }
    }
    [PunRPC]
    public void TakeDamage(int Damage,PhotonMessageInfo info)
    {
        this.currentHealth -= Damage;
        if(currentHealth<=0)
        {
            Eliminated(info.Sender.NickName);
            DeathRaceGameManager.instance.playersAlive--;
            if(DeathRaceGameManager.instance.playersAlive== 1)
            {
                winnerView(info.Sender.NickName);
            }
            
           
        }
    }
    [PunRPC]
    public void CreateHitEffects(Vector3 Position)
    {
        GameObject hitEffectGameobject = Instantiate(HiteffectPrefab,Position, Quaternion.identity);
        Destroy(hitEffectGameobject, 0.2f); 
    }



    [PunRPC]
    public void winnerView(string name)
    {

        GameObject winnerText = GameObject.Find("winnerText");
        winnerText.GetComponent<Text>().text = name + "Has won the death race";
        
    }
    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }
    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }
    void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code == (byte)RaiseEventCode.EliminatedCode)
        {
            object[] data = (object[])photonEvent.CustomData;
            string killer = (string)data[1];
            string killed = (string)data[0];
            KillText = GameObject.Find("KillText").GetComponent<Text>();
            KillText.GetComponent<Text>().text = killed + " is killed by " + killer;
           

        }
    }
    
    public void Eliminated(string KilledBy)
    {
        GetComponent<PlayerSetup>().camera.transform.parent = null;
        GetComponent<VehicleMovement>().enabled = false;
        this.gameObject.SetActive(false);
        
        string nickName = photonView.Owner.NickName;
        string Killer = KilledBy;
        object[] data = new object[] { nickName,KilledBy};
        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };
        SendOptions sendOption = new SendOptions
        {
            Reliability = false
        };

        PhotonNetwork.RaiseEvent((byte)RaiseEventCode.EliminatedCode, data, raiseEventOptions, sendOption);
        
    }
}
