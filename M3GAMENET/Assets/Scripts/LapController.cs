﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
public class LapController : MonoBehaviourPunCallbacks
{
    public List<GameObject> lapTriggers = new List<GameObject>();
    public enum RaiseEventCode
    {
        WhoFinishedEventCode=0 
    }
    private int FinishOrder = 0; 
    // Start is called before the first frame update
    void Start()
    {
        foreach (GameObject go in RacingGameManager.instance.lapTriggers)
        {
            lapTriggers.Add(go);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(lapTriggers.Contains(other.gameObject))
        {
            int indexOfTrigger = lapTriggers.IndexOf(other.gameObject);
            lapTriggers[indexOfTrigger].SetActive(false); 


        }
        if(other.gameObject.tag=="FinishTrigger")
        {
            GameFinish();
        }
    }
    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }
    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }
    void OnEvent(EventData photonEvent)
    {
        if(photonEvent.Code==(byte)RaiseEventCode.WhoFinishedEventCode)
        {
            object[] data = (object[])photonEvent.CustomData;
            string NicknameFirstplace =(string)data[0];
            FinishOrder = (int)data[1];
            Debug.Log(NicknameFirstplace + " " + FinishOrder);

        }
    }
    public void GameFinish()
    {
        GetComponent<PlayerSetup>().camera.transform.parent = null;
        GetComponent<VehicleMovement>().enabled = false;
        FinishOrder++;
        string nickName = photonView.Owner.NickName;
        object[] data = new object[] { nickName, FinishOrder };
        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };
        SendOptions sendOption = new SendOptions
        {
            Reliability=false
        };

        PhotonNetwork.RaiseEvent((byte)RaiseEventCode.WhoFinishedEventCode, data,raiseEventOptions,sendOption);
    }
}
